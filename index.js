const express = require('express')
const app = express()
const { engine } = require('express-handlebars')
const PORT = 8000
const uid = require('uid2')
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);


const data = []

app.use(express.urlencoded({ extended: true }))
app.use(express.json())


// Handlebars

app.engine('handlebars', engine())
app.set('view engine', 'handlebars')
app.set('views', './views')


app.get('/', (req, res) => {
  res.render('index')   
})


app.post('/products', async(req, res) => {
    const id = uid(10)
    const name = req.body
    io.emit('new product', name)
    res.status(201).json({ status: `Product saved with Id: ${id}` })
})

io.on('connection', (socket) => {
    socket.on('new product', (name) => {
      data.push({ id: uid(10),product: name })
      io.emit('update product', name)
    });
  });
  
  server.listen(PORT, () => {
    console.log('listening on: 8000');
  });